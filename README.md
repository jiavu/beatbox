# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

# Notes #

## Stack ##
MEAN, will check **nest** for Express and NodeJS

## Folder structures ##
### Example 1
root
- server
    - node_modules
    - package.json
    - ...
- client
    - node_modules
    - package.json
    - ...
- public (stuff to be serverd by nginx/proxy/server)
- config
- database
- README.md

### Example 2
root
- app (server stuff, so maybe 'server')
- models
- middlewares
- routes
- src (Angular CLI)
- dist (Angular CLI)
- server.js (or app.js, serves the dist folder, and connects to routes/models/middlewares in /app folder)
- config.js (configs for server.js)
- all other files: needed package.json, angular-cli.json etc
- README.md