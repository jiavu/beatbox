import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
// import { filter, map, take, tap } from 'rxjs/operators';
import { AudioBufferLoaderService } from './services/audio-buffer-loader/audio-buffer-loader.service';
import { MixerService } from './services/mixer/mixer.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent implements OnInit, OnDestroy {
  public audioWorkletsInitialized: Promise<boolean>;
  // public defaultSamplesLoaded: Promise<boolean>;

  constructor(
    private readonly audioBufferLoaderService: AudioBufferLoaderService,
    private readonly mixerService: MixerService,
  ) { }

  ngOnInit(): void {
    this.audioWorkletsInitialized = this.mixerService.audioCtx
      .audioWorklet.addModule('/assets/VU-meter/vumeter-processor.js')
      .then(() => true, error => {
        console.error(error);
        return false;
      });

/*     this.defaultSamplesLoaded =
      this.audioBufferLoaderService.getAudioBuffers$().pipe(
        filter(buffers => !!buffers),
        take(1),
        tap(() => console.log('aldkmqwlkdmnlkj')),
        map(() => true),
      ).toPromise(); */

    window.addEventListener('keydown', event => {
      // space and arrow keys
      if ([32, 37, 38, 39, 40].includes(event.keyCode)) {
        event.preventDefault();
      }
    }, false);

    this.audioBufferLoaderService.addAudioBuffers(
      [
        {name: 'Kick', dataUrl: '../../../assets/Tr808 kick.wav'},
        {name: 'Snare', dataUrl: '../../../assets/Tr808SnrareSnap.wav'},
        {name: 'Clap', dataUrl: '../../../assets/Tr808 handclap.wav'},
        {name: 'Hihat', dataUrl: '../../../assets/TechnoHh.wav'},
        /* {name: '1.5dB', dataUrl: '../../../assets/testtoene/test_1.5.wav'},
        {name: '0.0dB', dataUrl: '../../../assets/testtoene/0.0.wav'},
        {name: '-0.3dB', dataUrl: '../../../assets/testtoene/-0.3.wav'},
        {name: '-2.5dB', dataUrl: '../../../assets/testtoene/-2.5.wav'},
        {name: '-5.0dB', dataUrl: '../../../assets/testtoene/-5.wav'},
        {name: '-7.5dB', dataUrl: '../../../assets/testtoene/-7.5.wav'},
        {name: '-10.0dB', dataUrl: '../../../assets/testtoene/-10.wav'}, */
      ]
    );
  }

  ngOnDestroy(): void {
    this.mixerService.audioCtx.close();
  }
}
