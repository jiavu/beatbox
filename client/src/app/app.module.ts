import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { LetDirective } from './directives/let.directive';
import { AppComponent } from './app.component';
import { SequencerComponent } from './components/sequencer/sequencer.component';
import { SessionSettingsComponent } from './components/session-settings/session-settings.component';
import { MixerComponent } from './components/mixer/mixer.component';
import { AddTrackComponent } from './components/add-track/add-track.component';
import { PeakComponent } from './components/analyzer/peak/peak.component';
import { BlurDirective } from './directives/blur.directive';
import { ModalComponent } from './components/modal/modal.component';
import { SynthEditorComponent } from './components/instrumentEditors/synth-editor/synth-editor.component';
import { PianoRollComponent } from './components/piano-roll/piano-roll.component';
import { ArrangerComponent } from './components/arranger/arranger.component';
import { TransportComponent } from './components/transport/transport.component';
import { FirstLetterUppercasePipe } from './pipes/first-letter-uppercase.pipe';


@NgModule({
  declarations: [
    LetDirective,
    AppComponent,
    SequencerComponent,
    SessionSettingsComponent,
    MixerComponent,
    AddTrackComponent,
    PeakComponent,
    BlurDirective,
    ModalComponent,
    SynthEditorComponent,
    PianoRollComponent,
    ArrangerComponent,
    TransportComponent,
    FirstLetterUppercasePipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule { }
