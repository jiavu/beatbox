export enum VuMeterMode {
  RMS = 'rms',
  MAXPEAK = 'maxPeak',
}

export default class VUMeterNode extends AudioWorkletNode {

  // States in AudioWorkletNode
  private _updateIntervalInMS: number;
  private _mode: VuMeterMode;
  private _volume = 0;

  constructor(
    context: AudioContext,
    mode: VuMeterMode = VuMeterMode.MAXPEAK,
    updateIntervalInMS: number = 16.67,
  ) {
    super(context, 'vumeter', {
      numberOfInputs: 1,
      numberOfOutputs: 0,
      // channelCount: 2,
      channelCount: 1,
      processorOptions: { updateIntervalInMS, mode },
    });

    this._updateIntervalInMS = updateIntervalInMS;
    this._mode = mode;

    // Handels updated values from AudioWorkletProcessor
    this.port.onmessage = (event: MessageEvent) => {
      if ('number' === typeof event.data.volume) {
        this._volume = event.data.volume;
      }
    };

    this.port.start();
  }

  public get updateInterval(): number {
    return this._updateIntervalInMS;
  }
  public set updateInterval(updateIntervalInMS: number) {
    this._updateIntervalInMS = updateIntervalInMS;
    this.port.postMessage({updateIntervalInMS});
  }

  public get mode(): VuMeterMode {
    return this._mode;
  }
  public set mode(mode: VuMeterMode) {
    this._mode = mode;
  }

  public get volume(): number {
    return this._volume;
  }

  /* draw(): void {
    // to execute every |this._updateIntervalInMS| milliseconds
  } */
}
