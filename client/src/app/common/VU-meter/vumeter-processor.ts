// Can't use this .ts-file atm. It seems that audioWorklet.addModule() can
// load .js only and it has to be provided publicly.
// I would have to find a way to compile and provide this file AOT
// before app.component.ts is executed.
// ATM, a vumeter-processor.js is provided in the assets folder.

import { VuMeterMode } from './vumeter-node';


interface AudioWorkletProcessor {
  readonly port: MessagePort;
  process(
    inputs: Float32Array[][],
    outputs: Float32Array[][],
    parameters: Record<string, Float32Array>
  ): boolean;
}

declare const AudioWorkletProcessor: {
  prototype: AudioWorkletProcessor;
  new (options?: AudioWorkletNodeOptions): AudioWorkletProcessor;
};

declare function registerProcessor(
  name: string,
  processorCtor: (new (
    options?: AudioWorkletNodeOptions
  ) => AudioWorkletProcessor) & {
    parameterDescriptors?: AudioParamDescriptor[];
  }
): unknown;

declare let sampleRate: number;


const SMOOTHING_FACTOR = .9;
const MINIMUM_VALUE = .00001;

registerProcessor('vumeter', class extends AudioWorkletProcessor {
  private _updateIntervalInMS: number;
  private _mode: VuMeterMode;
  private _volume = 0;
  private _nextUpdateFrame: number;

  constructor(options: AudioWorkletNodeOptions) {
    super();
    this._updateIntervalInMS = options.processorOptions.updateIntervalInMS;
    this._mode = options.processorOptions.mode;
    this._nextUpdateFrame = this._updateIntervalInMS;    // ?
    // this._nextUpdateFrame = this.intervalInFrames;

    this.port.onmessage = event => {
      if (event.data.updateIntervalInMS) {
        this._updateIntervalInMS = event.data.updateIntervalInMS;
      }
    };
  }

  get intervalInFrames(): number {
    return this._updateIntervalInMS / 1000 * sampleRate;
  }

  process(inputs: Float32Array[][], outputs: Float32Array[][], parameters: Record<string, Float32Array>): boolean {
    const input = inputs[0];

    // if (input.length > 0) {

    // possibility to get 2 channels of Stereo?
    const samples = input[0];
    let volume: number;

    if (samples) {
      switch (this._mode) {
        case VuMeterMode.MAXPEAK:
          volume = Math.max(...samples);
          break;
        case VuMeterMode.RMS:
          // Calculated the squared-sum.
          const squaredSum = samples.map(sample => sample ** 2).reduce((a, b) => a + b);
          // Calculate the RMS level.
          volume = Math.sqrt(squaredSum / samples.length);
          break;
      }
    }

    //  Update the volume.
    this._volume = Math.max(volume, this._volume * SMOOTHING_FACTOR);
    if (MINIMUM_VALUE > this._volume) this._volume = 0;

    // Update and sync the volume property with the main thread.
    this._nextUpdateFrame -= samples ? samples.length : 128;

    if (this._nextUpdateFrame < 0) {
      this._nextUpdateFrame += this.intervalInFrames;
      this.port.postMessage({volume: this._volume});
    }

    // }

    return true;

    // Keep on processing if the volume is above a threshold, so that
    // disconnecting inputs does not immediately cause the meter to stop
    // computing its smoothed value.
    // return this._volume >= MINIMUM_VALUE;
  }
});
