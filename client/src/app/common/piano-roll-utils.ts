import { PianoRollKey } from '../models/models';

/*
Range: C-2 bis G8
440Hz = A3
220Hz = A2
110Hz = A1
55Hz = A0
27.5Hz = A-1
13.75Hz = A-2
*/


export class PianoRollUtils {
  static readonly pianoRollKeys: PianoRollKey[] = [
    {keyName: 'c', caption: 'C', color: 'white'},
    {keyName: 'c#', caption: 'C#', color: 'black'},
    {keyName: 'd', caption: 'D', color: 'white'},
    {keyName: 'd#', caption: 'D#', color: 'black'},
    {keyName: 'e', caption: 'E', color: 'white'},
    {keyName: 'f', caption: 'F', color: 'white'},
    {keyName: 'f#', caption: 'F#', color: 'black'},
    {keyName: 'g', caption: 'G', color: 'white'},
    {keyName: 'g#', caption: 'G#', color: 'black'},
    {keyName: 'a', caption: 'A', color: 'white'},
    {keyName: 'bb', caption: 'Bb', color: 'black'},
    {keyName: 'b', caption: 'B', color: 'white'},
  ];

  static readonly keyMap = new Map([
    ['cb', 246.942],
    ['c', 261.626],
    ['c#', 277.183],
    ['db', 277.183],
    ['d', 293.665],
    ['d#', 311.127],
    ['eb', 311.127],
    ['e', 329.628],
    ['e#', 349.228],
    ['fb', 329.628],
    ['f', 349.228],
    ['f#', 369.994],
    ['gb', 369.994],
    ['g', 391.995],
    ['g#', 415.305],
    ['ab', 415.305],
    ['a', 440],
    ['a#', 466.164],
    ['bb', 466.164],
    ['b', 493.883],
    ['b#', 523.251],
  ]);

  static getOctavesArray(from = -2, to = 7): number[] {
    const arr = [];
    for (let i = from; i <= to; i++) {
      arr.push(i);
    }
    return arr;
  }

  /**
   * Should be used in range: C-2 to G8, let's say B7
   * @param keyName - key and octave, e. g. 'A+3'
   * @returns Hz as number (float)
   */
  static getFrequency(keyName: string): number {
    if (/^[cdefgab][#b]?(-[0-3]|\+[0-8])$/gi.test(keyName)) {
      const octave: number = +keyName.slice(keyName.length - 2);
      const pitchOctavesFactor: number = 2 ** (octave - 3);
      const key: string = keyName.slice(0, keyName.length - 2).toLowerCase();
      return this.keyMap.get(key) * pitchOctavesFactor;
    }
  }
}
