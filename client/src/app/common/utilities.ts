import { AbstractControl, ValidationErrors } from '@angular/forms';

interface SessionValidateParams {
  maxBars: number;
  minBpm: number;
  maxBpm: number;
}

export const isInt = (numb: number): boolean => numb % 1 === 0;

export class Utils {
  static isPlaying(htmlAudioElement: HTMLAudioElement): boolean {
    return !!(
      htmlAudioElement.currentTime > 0 &&
      !htmlAudioElement.paused &&
      !htmlAudioElement.ended &&
      htmlAudioElement.readyState > 2
    );
  }

  static isIntegerValidator(control: AbstractControl): ValidationErrors | null {
    return !isInt(control.value) ? {integer: true} : null;
  }

  static roundToXDecimals(value: number, decimals: number = 2): number {
    return Math.round((value + Number.EPSILON) * 10 ** decimals) / 10 ** decimals;
  }

  static findNewId(existing: number[]): number {
    for (let i = 1; i <= existing.length + 1; i++) {
      if (!existing.includes(i)) {
        return i;
      }
    }
  }

  static getArrayFromN(n: number): number[] {
    return Array.of(...Array(n)).map((entry, idx) => idx);
  }

  static sessionDataisValid(session: any, conf: SessionValidateParams): boolean {
    const { maxBars, minBpm, maxBpm } = conf;

    try {
      const propIsMissing = [
        'name', 'bars', 'bpm', 'measure', 'tracks', 'instruments',
        'mixerChannels', 'sequenceIds', 'arrangement',

      ].some(propName => !Object.keys(session).includes(propName));

      if (propIsMissing) return false;

      const nameValid = 'string' === typeof session.name && session.name.length;
      const barsValid = 'number' === typeof session.bars &&
        0 < session.bars && maxBars >= session.bars && isInt(session.bars);
      const bpmValid = 'number' === typeof session.bpm &&
        minBpm < session.bpm && maxBpm >= session.bpm;
      const measureValid = 'number' === typeof session.measure.beats &&
        1 <= session.measure.beats && 16 >= session.measure.beats &&
        isInt(session.measure.beats) &&
        'number' === typeof session.measure.noteValue &&
        1 <= session.measure.noteValue && isInt(session.measure.noteValue);
      const tracksValid = Array.isArray(session.tracks) &&
        (session.tracks as any[]).every(track => {
          return 'number' === typeof track.trackId &&
            'string' === typeof track.name &&
            'number' === typeof track.position &&
            'string' === typeof track.instrument &&
            'number' === typeof track.instrumentId &&
            'boolean' === typeof track.pianoRoll &&
            Array.isArray(track.sequences) &&
            (track.sequences as any[]).every(entry => {
              return Array.isArray(entry) && 2 === entry.length &&
                'number' === typeof entry[0] && Array.isArray(entry[1]);
            });
        });
      const instrValid = 'object' === typeof session.instruments &&
        null !== session.instruments;
      const mixerChannelsValid = Array.isArray(session.mixerChannels) &&
        session.mixerChannels.every(channel => [
          'channelId', 'channelName', 'fx', 'gain', 'muted',
        ].every(propName => Object.keys(channel).includes(propName)));
      const seqIdsValid = Array.isArray(session.sequenceIds) &&
        session.sequenceIds.every(id => 'number' === typeof id);
      const arrangementValid = Array.isArray(session.arrangement) &&
        session.arrangement.every(entry => {
          return Array.isArray(entry) && 2 === entry.length &&
            entry.every(e => 'number' === typeof e);
        });

      const checks = {
        nameValid, barsValid, bpmValid, measureValid, tracksValid, instrValid,
        mixerChannelsValid, seqIdsValid, arrangementValid,
      };

      Object.entries(checks).forEach(([k, v]) => {
        if (!v) console.error('!' + k);
      });

      return Object.values(checks).every(check => !!check);
    }

    catch {
      return false;
    }
  }
}
