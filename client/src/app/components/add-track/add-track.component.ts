import { ChangeDetectionStrategy, Component, ElementRef, OnChanges, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { forkJoin, Observable, of, Subscription } from 'rxjs';
import { map, take, tap } from 'rxjs/operators';

import { Utils } from 'src/app/common/utilities';
import { ChannelType, Instrument, Option, Track } from 'src/app/models/models';
import { AudioBufferLoaderService } from 'src/app/services/audio-buffer-loader/audio-buffer-loader.service';
import { MixerService } from 'src/app/services/mixer/mixer.service';
import { SessionService } from 'src/app/services/session/session.service';
import { SynthService } from 'src/app/services/synth/synth.service';
import { TracksService } from 'src/app/services/tracks/tracks.service';
import { environment } from 'src/environments/environment';

enum FieldName {
  NAME = 'name',
  INSTRUMENT = 'instrument',
  SAMPLE = 'sample',
}


@Component({
  selector: 'app-add-track',
  templateUrl: './add-track.component.html',
  styleUrls: ['./add-track.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddTrackComponent implements OnInit, OnDestroy, OnChanges {
  @ViewChild('instr')
  set focusInstrSelect(select: ElementRef<HTMLElement>) {
    if (this.open && !this.instrSelect) {
      select.nativeElement.focus();
      this.instrSelect = select;
    } else if (!this.open) {
      this.instrSelect = null;
    }
  }

  public readonly FieldName = FieldName;
  public readonly Instrument = Instrument;
  private _open = false;
  private instrSelect: ElementRef;

  public samples$: Observable<Option<number>[]>;
  private samples: Option<number>[];

  public get open(): boolean {
    return this._open;
  }
  public set open(open: boolean) {
    this._open = open;
  }
  public instruments: Option<Instrument>[] = [
    {
      caption: 'Sample',
      value: Instrument.SAMPLE,
    },
    {
      caption: 'Synth',
      value: Instrument.SYNTH,
    },
  ];

  public newTrackSettingsFormGroup = new FormGroup({
    [FieldName.NAME]: new FormControl(),
    [FieldName.INSTRUMENT]: new FormControl(null, Validators.required),
    [FieldName.SAMPLE]: new FormControl(),
  });

  private subscriptions = new Subscription();

  get changeDetectionRuns(): string {
    if (environment.logChangeDetection) console.log('add-track view updating');
    return '';
  }

  constructor(
    private readonly audioBufferLoaderService: AudioBufferLoaderService,
    private readonly mixerService: MixerService,
    private readonly sessionService: SessionService,
    private readonly synthService: SynthService,
    private readonly tracksService: TracksService,
  ) { }

  ngOnChanges() {
    if (environment.logChangeDetection) console.warn('change in add-track component');
  }

  ngOnInit(): void {
    /* this.newTrackSettingsFormGroup.patchValue({
      [FieldName.NAME]: 'test',
    [FieldName.INSTRUMENT]: Instrument.SYNTH,
    });
    this.addTrack(); */

    this.samples$ = this.audioBufferLoaderService.getAudioBuffers$()
      .pipe(
        map(obj => Object.entries(obj).map(([id, sample]) => ({
        caption: sample.name,
        value: +id,
      }))),
      tap(samples => this.samples = samples),
      );

    const instrumentChangesSubscription = this.getFormCtrl(FieldName.INSTRUMENT)
      .valueChanges.subscribe(val => {

        const requiredFields: FormControl[] = ((instr: Instrument) => {
          switch (instr) {
            case Instrument.SAMPLE:
              return [this.getFormCtrl(FieldName.SAMPLE)];
            case Instrument.SYNTH:
              return [];
            default: return [];
          }
        })(val);

        // Set a Track name?
        if ([Instrument.SYNTH].includes(val)) {
          this.getFormCtrl(FieldName.NAME).setValue(val);
        }

        requiredFields.forEach(field => {
          field.setValidators(Validators.required);
          field.updateValueAndValidity({emitEvent: false});
        });

        Object.values(this.newTrackSettingsFormGroup.controls).forEach(ctrl => {
          if (requiredFields.includes(ctrl as FormControl)) return;
          ctrl.clearValidators();
          ctrl.updateValueAndValidity({emitEvent: false});
        });

        this.newTrackSettingsFormGroup.updateValueAndValidity();
      });

    const sampleChangesSubscription = this.getFormCtrl(FieldName.SAMPLE)
      .valueChanges.subscribe(val => {
        const trackName = val ?
          this.samples.find(sample => +val === sample.value)?.caption : null;

        this.getFormCtrl(FieldName.NAME).setValue(trackName);
      });

    this.subscriptions.add(instrumentChangesSubscription);
    this.subscriptions.add(sampleChangesSubscription);
  }

  public getValueOf(fieldname: FieldName): any {
    return this.newTrackSettingsFormGroup.get(fieldname).value;
  }

  public getFormCtrl(fieldname: FieldName): FormControl {
    return this.newTrackSettingsFormGroup.get(fieldname) as FormControl;
  }

  public toggleOpen(): void {
    this.open = !this.open;
  }

  public addTrack(): void {
    if (this.newTrackSettingsFormGroup.invalid) return;

    const newTrackData = this.newTrackSettingsFormGroup.value;

    const trackStatesMap = this.tracksService.trackStatesMap;
    const trackIds = Array.from(trackStatesMap.keys());
    const newId = Utils.findNewId(trackIds);
    const name = newTrackData[FieldName.NAME] || `${ChannelType.TRACK}_${newId}`;
    const instrument = newTrackData[FieldName.INSTRUMENT];
    const getsPianoRoll = [Instrument.SYNTH];
    const newTrack: Track = {
      name,
      position: trackIds.length + 1,
      instrument,
      pianoRoll: getsPianoRoll.includes(instrument),
      trackId: newId,
    } as Track;

    // Set instrument, create if necessary. Get Observable<instrumentId>:
    const instrumentIdObservable = (() => {
      switch (instrument) {
        case Instrument.SAMPLE: return of(+newTrackData[FieldName.SAMPLE]);
        case Instrument.SYNTH: return this.synthService.addSynth().pipe(
          map(newSynth => {
            this.sessionService.updateInstrument(Instrument.SYNTH, newSynth);
            return newSynth.id;
          }));
      }
    })();

    forkJoin([
      of(trackStatesMap),
      instrumentIdObservable.pipe(
        map(instrumentId => ({...newTrack, instrumentId})),
      ),
      this.mixerService.addMixerChannel(ChannelType.TRACK, newId, {name}),
      this.sessionService.sequenceIds$.pipe(take(1)),

    ]).pipe(take(1)).subscribe(([tracks, newTrack, mixerChannel, seqIds]) => {
      newTrack.destinationNode = mixerChannel.inputGainNode;
      newTrack.sequences = new Map(seqIds.map(seqId => [seqId, new Map()]));
      tracks.set(newTrack.trackId, newTrack);

      this.tracksService.trackStatesMap = tracks;
      // because number of tracks changes:
      this.tracksService.tracksChangesSubject.next();

      this.newTrackSettingsFormGroup.reset();
      this.newTrackSettingsFormGroup.updateValueAndValidity();
      this.open = false;
    });
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

}
