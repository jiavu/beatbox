import { AfterViewInit, ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, NgZone, OnChanges, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import VUMeterNode, { VuMeterMode } from 'src/app/common/VU-meter/vumeter-node';
import { MixerService } from 'src/app/services/mixer/mixer.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-peak',
  templateUrl: './peak.component.html',
  styleUrls: ['./peak.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PeakComponent implements OnInit, AfterViewInit, OnDestroy, OnChanges {
  @ViewChild('canvas') canvas: ElementRef<HTMLCanvasElement>;
  @Input() inputNode: AudioNode;
  @Input()
  set resetPeak(reset: boolean) {
    this.maxPeakInDb = -Infinity;
    this.clipped = false;
  }
  @Output() maxPeakChange = new EventEmitter<number>();

  private vuMeterNode = new VUMeterNode(this.mixerService.audioCtx, VuMeterMode.MAXPEAK);
  private requestId: number;
  private canvasCtx: CanvasRenderingContext2D;
  private fsHeight: number;
  public maxPeakInDb = -Infinity;
  public clipped: boolean;


  get changeDetectionRuns(): string {
    if (environment.logChangeDetection) console.log('peak component view updating');
    return '';
  }


  constructor(
    private readonly mixerService: MixerService,
    private readonly ngZone: NgZone,
  ) { }

  ngOnChanges() {
    if (environment.logChangeDetection)  console.warn('change in peak component');
  }

  ngOnInit(): void {
    this.inputNode.connect(this.vuMeterNode);
  }

  ngAfterViewInit(): void {
    this.canvasCtx = this.canvas.nativeElement.getContext('2d');
    this.fsHeight = (this.canvas.nativeElement.height / 120) * 100;
    this.ngZone.runOutsideAngular(() => this.tick() );
  }

  private tick(): void {
    const canvas = this.canvas.nativeElement;
    this.canvasCtx.clearRect(0, 0, canvas.width, canvas.height);

    const volume = this.vuMeterNode.volume;
    const peakInDb = 20 * Math.log10(volume);

    if (this.maxPeakInDb < peakInDb) {
      this.maxPeakInDb = peakInDb;

      this.ngZone.run(() => {
        this.maxPeakChange.next(this.maxPeakInDb);
        this.clipped = 0 <= this.maxPeakInDb;
      });
    }

    const rectHeight = volume * this.fsHeight;

    this.canvasCtx.fillStyle = '#000000';
    this.canvasCtx.fillRect(0, canvas.height, canvas.width, -rectHeight);

    this.requestId = requestAnimationFrame(this.tick.bind(this));
  }

  ngOnDestroy(): void {
    cancelAnimationFrame(this.requestId);
  }
}

/*
  volume from vumeter-processor will be a float >= 0.
  1 should be 0dB.
  The channel slider has a range of 0 to 120 steps.
  100 of 120 should match volume of 1.

  (canvas.height / 120) * 100   // entspricht volume of 1
  ===> rectHeight = volume * fsHeight / 1

  peak in dB:
  "0 dB is the loudest possible sound, -10 dB is a 10th of that, etc."

  const peakInDb = 10 * Math.log10(currenPeak / 1);
*/
