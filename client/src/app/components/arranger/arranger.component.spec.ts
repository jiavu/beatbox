import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArrangerComponent } from './arranger.component';

describe('ArrangerComponent', () => {
  let component: ArrangerComponent;
  let fixture: ComponentFixture<ArrangerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArrangerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArrangerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
