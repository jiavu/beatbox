import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { Utils } from 'src/app/common/utilities';
import { ArrangerService } from 'src/app/services/arranger/arranger.service';
import { SessionService } from 'src/app/services/session/session.service';

interface DragItem {
  label: string;
  seqId: number;
  removeAt?: number;
}

type ArrangementView = Map<number, DragItem|null>;

@Component({
  selector: 'app-arranger',
  templateUrl: './arranger.component.html',
  styleUrls: ['./arranger.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ArrangerComponent implements OnInit {
  public dragItems$: Observable<DragItem[]>;
  public updateView$: Observable<void>;

  public dragItems: DragItem[];
  public positions = [1];
  /** key: position number */
  public arrangement: ArrangementView;

  private dragging: DragItem;

  constructor(
    private readonly arrangerService: ArrangerService,
    private readonly sessionService: SessionService,
  ) { }

  ngOnInit(): void {
    this.dragItems$ = this.sessionService.sequenceIds$.pipe(
      map(ids => ids.map(seqId => ({seqId, label: 'Seq. ' + seqId}))),
      tap(dragItems => this.dragItems = dragItems),
    );

    this.updateView$ = this.arrangerService.getUpdateArrangement$().pipe(
      map(arrangement => {
        if (!arrangement?.length) {
          this.arrangement = new Map();
          return;
        }

        const lastPosition = Math.max(...arrangement.map(entry => entry[0]));

        this.positions = Utils.getArrayFromN(lastPosition + 1).map(numb => numb + 1);

        this.arrangement = new Map(
          arrangement.map(([positionNumber, seqId]) => [
            positionNumber, {label: 'Seq. ' + seqId, seqId}
          ])
        );
      }),
    );
  }

  public dragStart($event: DragEvent, dragItem: DragItem, removeAt?: number): void {
    this.dragging = dragItem;
    if (removeAt) this.dragging.removeAt = removeAt;
  }

  public dragEnd($event: DragEvent, dragItem: DragItem): void {
    this.dragging = null;
  }

  public dragOver($event: DragEvent): void {
    if (this.dragging) $event.preventDefault();
  }

  public drop($event: DragEvent, pos?: number, remove?: boolean): void {
    if (remove && this.dragging.removeAt) {
      this.arrangement.delete(this.dragging.removeAt);
      this.arrangerService.arrangementMap.delete(this.dragging.removeAt);

    } else if (pos) {
      const draggedItem = this.dragItems.find(item => this.dragging === item);
      this.arrangement.set(pos, draggedItem);
      this.arrangerService.arrangementMap.set(pos, draggedItem.seqId);

      if (pos === this.positions.length) {
        this.positions.push(this.positions.length + 1);
      }
    }
    this.dragging = null;
  }
}
