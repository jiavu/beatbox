import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SynthEditorComponent } from './synth-editor.component';

describe('SynthEditorComponent', () => {
  let component: SynthEditorComponent;
  let fixture: ComponentFixture<SynthEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SynthEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SynthEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
