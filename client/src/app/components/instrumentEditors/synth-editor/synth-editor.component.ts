import { ChangeDetectionStrategy, Component, Input, OnChanges, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { map, take, tap } from 'rxjs/operators';
import { Utils } from 'src/app/common/utilities';

import { Instrument, Synth } from 'src/app/models/models';
import { SessionService } from 'src/app/services/session/session.service';
import { SynthService } from 'src/app/services/synth/synth.service';
import { environment } from 'src/environments/environment';

enum FieldName {
  TYPE = 'type',
  ADSR = 'adsr',
}

enum Adsr {
  ATTACK = 'attack',
  DECAY = 'decay',
  SUSTAIN = 'sustain',
  RELEASE = 'release',
}

interface SynthSettingsForm {
  [FieldName.ADSR]: {[key in Adsr]: number};
  [FieldName.TYPE]: OscillatorType;
}


@Component({
  selector: 'app-synth-editor',
  templateUrl: './synth-editor.component.html',
  styleUrls: ['./synth-editor.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SynthEditorComponent implements OnInit, OnDestroy, OnChanges {
  @Input() instrumentId: number;

  // private synths: Synths;
  public selectedSynth$: Observable<Synth>;
  private selectedSynth: Synth;
  private valueChangesSubscription: Subscription;

  public synthSettingsFormGroup = new FormGroup({
    [FieldName.ADSR]: new FormGroup({
      [Adsr.ATTACK]: new FormControl(),
      [Adsr.DECAY]: new FormControl(),
      [Adsr.SUSTAIN]: new FormControl(),
      [Adsr.RELEASE]: new FormControl(),
    }),
    [FieldName.TYPE]: new FormControl(),
  });

  public readonly Fieldname = FieldName;
  public readonly oscillatorTypes: OscillatorType[] = [
    'sine', 'sawtooth', 'square', 'triangle',
  ];
  public readonly adsr: Adsr[] = [
    Adsr.ATTACK, Adsr.DECAY, Adsr.SUSTAIN, Adsr.RELEASE,
  ];
  public maxSustainGain: number;

  get changeDetectionRuns(): string {
    if (environment.logChangeDetection) console.log('synth editor component view updating');
    return '';
  }

  constructor(
    private readonly sessionService: SessionService,
    private readonly synthService: SynthService,
  ) { }

  ngOnChanges() {
    if (environment.logChangeDetection) console.warn('change in synth editor component');
  }

  ngOnInit(): void {
    this.maxSustainGain = this.synthService.maxGain * 100;

    this.selectedSynth$ = this.synthService.getSynths$().pipe(
      take(1),
      map(synths => synths[this.instrumentId]),
      tap(synth => {
        this.setFormValues(synth);
        this.selectedSynth = synth;
      }),
    );

    this.valueChangesSubscription = this.synthSettingsFormGroup.valueChanges
      .subscribe((value: SynthSettingsForm) => {
        const synth: Synth = {
          ...this.selectedSynth,
          // id: this.instrumentId,
          adsr: [
            Utils.roundToXDecimals(value[FieldName.ADSR][Adsr.ATTACK] / 100, 4),
            Utils.roundToXDecimals(value[FieldName.ADSR][Adsr.DECAY] / 100, 4),
            Utils.roundToXDecimals(value[FieldName.ADSR][Adsr.SUSTAIN] / 100, 4),
            Utils.roundToXDecimals(value[FieldName.ADSR][Adsr.RELEASE] / 100, 4),
          ],
          type: value[FieldName.TYPE],
        };
        this.synthService.changeSynthSetting(synth);
        this.sessionService.updateInstrument(Instrument.SYNTH, synth);
      });
  }

  private setFormValues(synth: Synth): void {
    const adsr: {[key in Adsr]: number} = {
      attack: synth.adsr[0] * 100,
      decay: synth.adsr[1] * 100,
      sustain: synth.adsr[2] * 100,
      release: synth.adsr[3] * 100,
    };
    this.synthSettingsFormGroup.setValue({type: synth.type, adsr}, {emitEvent: false});
  }

  onModalClose(): void {
    this.sessionService.openInstrumentEditorSubject.next(null);
  }

  ngOnDestroy(): void {
    this.valueChangesSubscription.unsubscribe();
  }
}
