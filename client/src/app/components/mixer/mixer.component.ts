import { ChangeDetectionStrategy, Component, OnChanges, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Utils } from 'src/app/common/utilities';

import { MixerChannel } from 'src/app/models/models';
import { MixerService } from 'src/app/services/mixer/mixer.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-mixer',
  templateUrl: './mixer.component.html',
  styleUrls: ['./mixer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MixerComponent implements OnInit, OnChanges {
  public mixerChannelStates$: Observable<[string, MixerChannel][]>;

  public maxPeaks: Map<string, number> = new Map();
  public saveRessources = false;

  get changeDetectionRuns(): string {
    if (environment.logChangeDetection) console.log('mixer component view updating');
    return '';
  }

  constructor(
    private readonly mixerService: MixerService,
  ) { }

  ngOnChanges() {
    if (environment.logChangeDetection) console.warn('change in mixer component');
  }

  ngOnInit(): void {
    this.mixerChannelStates$ = this.mixerService.mixerChannelsMap$.pipe(
      map(mixerChannelsMap => Array.from(mixerChannelsMap)),
    );
  }

  public changeTrackVolume(trackId: string, $event): void {
    const absoluteValue = Utils.roundToXDecimals($event.target.value / 100, 4);
    this.mixerService.changeTrackVolume(trackId, absoluteValue);
    this.resetPeak(trackId);
  }

  public resetPeak(trackId: string): void {
    this.maxPeaks.set(trackId, -Infinity);
  }

  public isNegativeInf(value: number): boolean {
    return -Infinity === value;
  }

  public toggleMute(channelId: string, mute: boolean): void {
    this.mixerService.changeMute(channelId, !mute);
  }
}
