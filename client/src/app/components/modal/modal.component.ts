import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ModalComponent implements OnInit {
  @Input() modalTitle: string;
  @Input() keepInDom: boolean;
  @Output() modalClose = new EventEmitter<boolean>();
  @Input()
  set open(open: boolean) {
    this._open = open;
  }
  get open(): boolean {
    return this._open;
  }

  private _open: boolean;

  constructor() { }

  ngOnInit(): void {
  }

  closeModal(): void {
    this.open = false;
    this.modalClose.next(true);
  }

}
