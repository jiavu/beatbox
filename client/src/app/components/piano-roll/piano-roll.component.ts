import { ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { PianoRollUtils } from 'src/app/common/piano-roll-utils';
import { Utils } from 'src/app/common/utilities';
import { NotesMap, SequenceChanges, SequencePosition, SessionSettings, Track } from 'src/app/models/models';
import { SequencerService } from 'src/app/services/sequencer/sequencer.service';
import { environment } from 'src/environments/environment';

interface Key {
  keyNameOctave: string;
  frequency: number;
}


@Component({
  selector: 'app-piano-roll',
  templateUrl: './piano-roll.component.html',
  styleUrls: ['./piano-roll.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PianoRollComponent implements OnInit, OnDestroy, OnChanges {
  @ViewChild('scrollContainer') scrollContainer: ElementRef<HTMLElement>;
  @ViewChild('positionMarker')
  set positionMarker(element: ElementRef<HTMLElement>) {
    this.sequencerService.positionMarkerRefs.set(
      'pianoRollSequenceMarker', element.nativeElement
    );
  }
  @Input()
  set sessionSettings(settings: SessionSettings) {
    this._sessionSettings = settings;
  }
  get sessionSettings(): SessionSettings {
    return this._sessionSettings;
  }

  @Output() changes = new EventEmitter<SequenceChanges>();
  @Output() playNotes = new EventEmitter<{track: Track, notes: NotesMap}>();

  public pianoRollOpen$: Observable<Track>;
  public selectedSequence$: Observable<number>;

  public readonly pianoRollKeys = PianoRollUtils.pianoRollKeys.reverse();
  public readonly octaves = PianoRollUtils.getOctavesArray(-1).reverse();
  public readonly getArrayFromN = Utils.getArrayFromN;

  private track: Track;
  private selectedSequence: number;
  private showKeyNames: boolean;
  private _sessionSettings: SessionSettings;


  get changeDetectionRuns(): string {
    if (environment.logChangeDetection) console.log('Piano roll view updating');
    return '';
  }

  constructor(private readonly sequencerService: SequencerService) { }

  ngOnChanges() {
    if (environment.logChangeDetection) console.warn('change in piano roll component');
  }

  ngOnInit(): void {
    this.pianoRollOpen$ = this.sequencerService.pianoRollOpen$.pipe(
      tap(track => this.track = track),
      tap(track => {
        if (track) {
          const scrollElement = this.scrollContainer.nativeElement;
          scrollElement.scrollTo(0, scrollElement.scrollHeight * .25);
        }
      }),
    );

    this.selectedSequence$ = this.sequencerService.selectedSequence$
      .pipe(tap(seqId => this.selectedSequence = seqId));
  }

  public getKey(keyName: string, octave: number): Key {
    const keyNameOctave = keyName + (-1 < octave ? '+' : '') + octave;

    return {
      keyNameOctave,
      frequency: PianoRollUtils.getFrequency(keyNameOctave),
    };
  }

  public playNote(key: Key): void {
    const notes: NotesMap = new Map([
      [
        key.keyNameOctave,
        {
          frequency: key.frequency,
          length: .5,   // todo: change that to fract length...
        }
      ]
    ]);

    this.playNotes.next({track: this.track, notes});
  }

  public onModalClose(): void {
    this.sequencerService.pianoRollOpenSubject.next(null);
  }

  public showKeyName(keyName: string): boolean {
    return this.showKeyNames || /^c$/gi.test(keyName);
  }

  public isActive(seqPos: SequencePosition, key: Key): boolean {
    const sequencePos = seqPos.join('.');
    return !!this.track?.sequences.get(this.selectedSequence)
      .get(sequencePos)?.has(key.keyNameOctave);
  }

  public changeNote(seqPos: SequencePosition, key: Key, add: boolean): void {
    const sequencePos = seqPos.join('.');
    let notesMap = this.track.sequences.get(this.selectedSequence).get(sequencePos);

    if (add && !notesMap) notesMap = new Map();

    if (add) {
      const length = .5;  // todo: change that to fract length...

      notesMap.set(key.keyNameOctave, {
        key: key.keyNameOctave,
        length,
        frequency: key.frequency,
      });

    } else if (notesMap) {
      notesMap.delete(key.keyNameOctave);
    }

    if (!notesMap.size) {
      this.track.sequences.get(this.selectedSequence).delete(sequencePos);
    } else {
      this.track.sequences.get(this.selectedSequence).set(sequencePos, notesMap);
    }

    this.changes.next({
      trackId: this.track.trackId,
      sequenceId: this.selectedSequence,
      position: seqPos,
      notes: notesMap.size && notesMap,
    });
  }

  ngOnDestroy(): void {
    this.sequencerService.positionMarkerRefs.delete('pianoRollSequenceMarker');
    this.sequencerService.pianoRollOpenSubject.next(null);
  }
}
