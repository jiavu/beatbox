import { ChangeDetectionStrategy, Component, ElementRef, OnChanges, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';

import {
  Instrument,
  Note,
  SelectedInstrument,
  SequenceChanges,
  SequencePosition,
  SessionSettings,
  Track,
  TrackSequenceMap,
  TrackStatesMap,
} from '../../models/models';
import { Utils } from 'src/app/common/utilities';
import { SequencerService } from '../../services/sequencer/sequencer.service';
import { SessionService } from '../../services/session/session.service';
import { environment } from 'src/environments/environment';
import { TracksService } from 'src/app/services/tracks/tracks.service';


@Component({
  selector: 'app-sequencer',
  templateUrl: './sequencer.component.html',
  styleUrls: ['./sequencer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SequencerComponent implements OnInit, OnDestroy, OnChanges {
  @ViewChild('positionMarker')
  set positionMarker(element: ElementRef<HTMLElement>) {
    if (element) {
      this.sequencerService.positionMarkerRefs.set(
        'mainSequenceMarker', element.nativeElement
      );
    }
  }

  public readonly Instrument = Instrument;
  public readonly getArrayFromN = Utils.getArrayFromN;
  public readonly defaultNote: Note = {
    frequency: 440,
    length: .5,   // todo: change that to fract length...
  };

  public sessionSettings$: Observable<SessionSettings>;
  public sequenceIds$: Observable<number[]>;
  public openInstrumentEditor$: Observable<SelectedInstrument>;
  public selectedSequence$: Observable<number>;
  private openSubscriptions = new Subscription();

  public tracks: [number, Track][] = [];
  public selectedSequence: number;

  get changeDetectionRuns(): string {
    if (environment.logChangeDetection) console.log('sequencer component view updating');
    return '';
  }

  constructor(
    private readonly sequencerService: SequencerService,
    private readonly sessionService: SessionService,
    private readonly tracksService: TracksService,
  ) { }

  ngOnChanges() {
    if (environment.logChangeDetection) console.warn('change in sequencer component');
  }

  ngOnInit(): void {
    const updateTracksSubscription = this.tracksService.getTracksChanges$()
      .subscribe(this.onTracksChanged.bind(this));

    this.openSubscriptions.add(updateTracksSubscription);

    this.sequenceIds$ = this.sessionService.sequenceIds$;
    this.openInstrumentEditor$ = this.sessionService.openInstrumentEditor$;

    this.sequencerService.selectedSequenceSubject.next(1);
    this.selectedSequence$ = this.sequencerService.selectedSequence$.pipe(
      tap(seqId => this.selectedSequence = seqId),
    );

    this.sessionSettings$ = this.sessionService.sessionSettings$;
  }

  private onTracksChanged(trackStates: TrackStatesMap): void {
    this.tracksService.trackStatesMap = trackStates;
    this.tracks = Array.from(this.tracksService.trackStatesMap);
  }

  public isActive(sequence: TrackSequenceMap, sequencePos: SequencePosition): boolean {
    return sequence.has(sequencePos.join('.'));
  }

  public toggleIsActive(trackId: number, position: SequencePosition, isActive: boolean): void {
    const notes = isActive ? null : new Map([['xx', this.defaultNote]]);
    this.changeSequence({trackId, sequenceId: this.selectedSequence, position, notes});
  }

  public selectSequence(seqId: number): void {
    this.sequencerService.selectedSequenceSubject.next(+seqId);
  }

  public addSequence(): void {
    this.sessionService.addOrRemoveSequence(this.tracksService.trackStatesMap)
      .then(trackStatesMap => {
        this.tracksService.trackStatesMap = trackStatesMap;
        this.tracks = Array.from(trackStatesMap);
      });
  }

  public changeSequence(changes: SequenceChanges): void {
    const seqPos = changes.position.join('.');
    const track = this.tracksService.trackStatesMap.get(changes.trackId);

    if (changes.notes?.size) {
      track.sequences.get(changes.sequenceId).set(seqPos, changes.notes);
    } else {
      track.sequences.get(changes.sequenceId).delete(seqPos);
    }

    // update array version of tracks:
    const idx = this.tracks.findIndex(entry => track.trackId === entry[0]);
    if (-1 < idx) this.tracks[idx][1] = track;
  }

  public deleteTrack(trackId: number): void {
    const del = window.confirm('Delete track ?');
    if (del) {
      this.tracksService.trackStatesMap =
        this.sessionService.deleteTrack(this.tracksService.trackStatesMap, trackId);
      this.tracks = Array.from(this.tracksService.trackStatesMap);
    }
  }

  public editInstrument(track: Track): void {
    this.sessionService.openInstrumentEditorSubject.next({
      instrument: track.instrument,
      instrumentId: track.instrumentId,
    });
  }

  public openPianoRoll(track: Track): void {
    this.sequencerService.pianoRollOpenSubject.next(track);
  }

  ngOnDestroy(): void {
    this.sequencerService.positionMarkerRefs.delete('mainSequenceMarker');
    this.openSubscriptions.unsubscribe();
  }
}
