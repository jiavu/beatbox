import { ChangeDetectionStrategy, Component, OnChanges, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { map, mergeMap, take } from 'rxjs/operators';
import { Utils } from 'src/app/common/utilities';
import { SessionService } from 'src/app/services/session/session.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-session-settings',
  templateUrl: './session-settings.component.html',
  styleUrls: ['./session-settings.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SessionSettingsComponent implements OnInit, OnDestroy, OnChanges {
  private valueChangesSubscription: Subscription;

  public settingsFormGroup = new FormGroup({
    name: new FormControl(null, Validators.required),
    bpm: new FormControl(null, [
      Validators.required,
      Validators.min(this.sessionService.validationConf.minBpm),
      Validators.max(this.sessionService.validationConf.maxBpm),
    ]),
    bars: new FormControl(null, [
      Validators.required,
      Validators.min(1),
      Validators.max(this.sessionService.validationConf.maxBars),
      Utils.isIntegerValidator,
    ]),
    measure: new FormGroup({
      beats: new FormControl(null, [
        Validators.required,
        Validators.min(1),
        Validators.max(16),
        Utils.isIntegerValidator,
      ]),
      noteValue: new FormControl(null, [
        Validators.required,
        Utils.isIntegerValidator,
      ]),
    }),
  });

  get changeDetectionRuns(): string {
    if (environment.logChangeDetection) console.log('session settings component view updating');
    return '';
  }


  constructor(private sessionService: SessionService) { }

  ngOnChanges() {
    if (environment.logChangeDetection) console.warn('change in session settings component');
  }

  ngOnInit(): void {
    this.valueChangesSubscription = this.settingsFormGroup.valueChanges
      .pipe(
        mergeMap(value => this.sessionService.sessionSettings$
          .pipe(
            take(1),
            map(settings => ({...settings, ...value})),
          ),
        ),
      ).subscribe(value => {
        if (this.settingsFormGroup.valid) {
          value.bpm = +value.bpm;
          value.bars = +value.bars;
          value.measure.beats = +value.measure.beats;
          value.measure.noteValue = +value.measure.noteValue;
          this.sessionService.sessionSettingsSubject.next(value);
        }
      });

    this.loadSession();
  }

  public loadSession(newSession?: boolean): void {
    this.sessionService.requestSessionSettings(newSession).pipe(
      take(1),
      ).subscribe(settings => {
        this.sessionService.sessionSettingsSubject.next(settings);
        this.settingsFormGroup.patchValue(settings);
      });
  }

  public saveSession(): void {
    this.sessionService.saveSession();
  }

  public downloadSession(): void {
    this.saveSession();

    const dataStr = 'data:text/json;charset=utf-8,' +
      encodeURIComponent(localStorage.getItem('beatBoxSession'));

    const link = document.createElement('a');
    link.setAttribute('href', dataStr);
    link.setAttribute('download', 'beatBoxSession.json');
    link.click();
  }

  public loadSessionFile($event: InputEvent): void {
    const files = ($event.target as HTMLInputElement).files;

    if (!files.length) return;

    const reader = new FileReader();

    reader.onerror = err => console.error(err);

    reader.onload = event => {
      const data = JSON.parse(reader.result as string);

      if (!Utils.sessionDataisValid(data, this.sessionService.validationConf)) {
        return window.alert('Invalid data');
      }
      this.sessionService.initializeSessionSettings(data).pipe(
        take(1),
        ).subscribe(settings => {
          this.sessionService.sessionSettingsSubject.next(settings);
          this.settingsFormGroup.patchValue(settings);
        });
    };

    reader.readAsText(files[0]);
  }

  ngOnDestroy(): void {
    this.valueChangesSubscription.unsubscribe();
  }
}
