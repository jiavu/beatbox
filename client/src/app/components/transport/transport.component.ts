import { formatNumber } from '@angular/common';
import { Component, OnInit, ChangeDetectionStrategy, ViewChild, ElementRef, OnDestroy, OnChanges, NgZone } from '@angular/core';
import { fromEvent, Observable, Subscription } from 'rxjs';
import { filter, tap } from 'rxjs/operators';
import { Instrument, NotesMap, Playmode, SequencerSetup, Track } from 'src/app/models/models';
import { ArrangerService } from 'src/app/services/arranger/arranger.service';
import { AudioBufferLoaderService } from 'src/app/services/audio-buffer-loader/audio-buffer-loader.service';
import { SequencerService } from 'src/app/services/sequencer/sequencer.service';
import { SessionService } from 'src/app/services/session/session.service';
import { SynthService } from 'src/app/services/synth/synth.service';
import { TracksService } from 'src/app/services/tracks/tracks.service';
import { TransportService } from 'src/app/services/transport/transport.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-transport',
  templateUrl: './transport.component.html',
  styleUrls: ['./transport.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TransportComponent implements OnInit, OnChanges, OnDestroy {
  @ViewChild('elapsedTime')
  set elapsedTimeDisplay(display: ElementRef<HTMLElement>) {
    this.transportService.elapsedTimeDisplayRef = display.nativeElement;
  }
  @ViewChild('sequencePosition')
  set seqPosDisplay(display: ElementRef<HTMLElement>) {
    this.transportService.seqPosRef = display.nativeElement;
  }

  public play$: Observable<boolean>;
  private openSubscriptions = new Subscription();

  private setup: SequencerSetup;
  public playMode: Playmode = Playmode.SEQUENCE;
  private playing: boolean;
  private requestId: number;
  private startTime: number = null;
  private currentSequence: number;
  private selectedSequence: number;

  constructor(
    private readonly arrangerService: ArrangerService,
    private readonly audioBufferLoaderService: AudioBufferLoaderService,
    private readonly ngZone: NgZone,
    private readonly sequencerService: SequencerService,
    private readonly sessionService: SessionService,
    private readonly synthService: SynthService,
    private readonly tracksService: TracksService,
    private readonly transportService: TransportService,
  ) { }

  ngOnChanges() {
    if (environment.logChangeDetection) console.warn('change in transport component');
  }

  ngOnInit(): void {
    this.play$ = this.transportService.play$.pipe(
      tap(playing => {
        this.playing = playing;

        if (!playing) {
          cancelAnimationFrame(this.requestId);
          this.startTime = null;
          this.transportService.sequencePosition = null;
          this.sequencerService.positionMarkerRefs.forEach(element => {
            element.classList.add('hidden');
          });

        } else if (null === this.startTime) {
          this.sequencerService.positionMarkerRefs.forEach(element => {
            element.classList.remove('hidden');
          });

          this.ngZone.runOutsideAngular(() => {
            this.requestId = requestAnimationFrame(this.tick.bind(this));
          });
        }
      }),
    );

    const sessionSettingsChangesSubscr = this.sessionService.sessionSettings$
      .subscribe(settings => {
        this.setup = {
          measure: settings.measure,
          bpm: settings.bpm,
          bars: settings.bars,
        };
      });

    const selectedSequenceChangesSubscr = this.sequencerService.selectedSequence$
      .subscribe(seqId => {
        this.selectedSequence = seqId;
        if (Playmode.SEQUENCE === this.playMode) {
          this.currentSequence = seqId;
        }
      });

    const keyEventSubscription = fromEvent(document, 'keydown').pipe(
      filter((keyEvent: KeyboardEvent) => {
        return !['INPUT', 'SELECT'].includes(((keyEvent.target as Node).nodeName));
      }),
      ).subscribe((keyEvent: KeyboardEvent) => {
        if (' ' === keyEvent.key) this.togglePlay();
      });

    [
      sessionSettingsChangesSubscr,
      selectedSequenceChangesSubscr,
      keyEventSubscription,
    ]
      .forEach(subscr => {
        this.openSubscriptions.add(subscr);
      });
  }

  public togglePlay(): void {
    this.transportService.playSubject.next(!this.playing);
  }

  public togglePlayMode(): void {
    this.playMode = Playmode.SEQUENCE === this.playMode ?
      Playmode.ARRANGEMENT : Playmode.SEQUENCE;

    if (Playmode.ARRANGEMENT === this.playMode) {
      this.startTime = null;
    } else {
      this.currentSequence = this.selectedSequence;
    }
  }

  private tick(timestamp): void {
    if (this.startTime === null) this.startTime = timestamp;

    const elapsedTime = timestamp - this.startTime;
    this.transportService.elapsedTime = elapsedTime;

    this.transportService.elapsedTimeDisplayRef.innerText =
      formatNumber(elapsedTime, 'en', '1.0-0');

    const newPos =
      this.sequencerService.getPositions(this.setup, elapsedTime);

    const markerPosition =
      this.sequencerService.calcMarkerPosition(this.setup, elapsedTime);

    this.sequencerService.positionMarkerRefs.forEach(element => {
      element.style.left = markerPosition;
    });

    if (newPos.seqPos !== this.transportService.sequencePosition) {
      this.transportService.sequencePosition = newPos.seqPos;
      this.transportService.seqPosRef.innerText = newPos.seqPos;

      if (Playmode.ARRANGEMENT === this.playMode) {
        this.currentSequence =
          this.arrangerService.arrangementMap.get(newPos.arrangerPos);
      }

      // play sounds
      if (this.currentSequence) {
        this.tracksService.trackStatesMap.forEach(trackState => {
          if (trackState.sequences.get(this.currentSequence).has(newPos.seqPos)) {
            this.playNotes(
              trackState,
              trackState.sequences.get(this.currentSequence).get(newPos.seqPos)
            );
          }
        });
      }
    }

    this.requestId = requestAnimationFrame(this.tick.bind(this));
  }

  public playNotes(track: Track, notes: NotesMap): void {
    notes.forEach(note => {
      switch (track.instrument) {
        case Instrument.SAMPLE:
          this.audioBufferLoaderService.playAudioBuffer(
            track.instrumentId, track.destinationNode
          );
          break;
        case Instrument.SYNTH:
          this.synthService.playOscillator(
            track.instrumentId, note, track.destinationNode
          );
          break;
      }
    });
  }

  ngOnDestroy(): void {
    cancelAnimationFrame(this.requestId);
    this.openSubscriptions.unsubscribe();
  }

}
