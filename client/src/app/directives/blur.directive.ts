import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[appBlur]'
})
export class BlurDirective {

  @HostListener('click', ['$event.currentTarget'])
  onMouseDown(target: HTMLElement): void {
    target.blur();
  }

}
