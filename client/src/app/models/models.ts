/* SESSION SETUP */

export interface SequencerSetup {
  measure: Measure;
  bpm: number;
  bars: number;
}

export interface Measure {
  beats: number;
  noteValue: number;
}

export interface SessionSettings extends SequencerSetup {
  mixerChannels: MixerChannel[];
  name: string;
  tracks: Track[];
  instruments: Instruments;
  /** a list of the existing sequence ids */
  sequenceIds: number[];
  arrangement: [number, number][];
}

/* ARRANGEMENT */

/** key: position number. value: sequence id. */
export type ArrangementMap = Map<number, number>;


/* TRACKS */

export type TrackStatesMap = Map<number, Track>;

export interface Track {
  trackId: number;
  name: string;
  position: number;
  instrument: Instrument;
  instrumentId?: number;
  pianoRoll?: boolean;
  destinationNode?: GainNode;
  sequences: Map<number, TrackSequenceMap>;
}


/* SEQUENCE */

export enum Playmode {
  SEQUENCE = 'sequence',
  ARRANGEMENT = 'arrangement',
}

/** e. g. '1.2.1': [] */
export type TrackSequenceMap = Map<string, NotesMap>;

export type SequencePosition = [number, number, number];

/** e. g. 'a+3': Note */
export type NotesMap = Map<string, Note>;

export interface Note {
  /** e. g. 'a+3' ==> lowerCase */
  key?: string;
  frequency?: number;
  /** Default: 0 (play immediately) */
  /** seconds */
  start?: number;
  /** seconds */
  length: number;
  gain?: number;
}

export interface SequenceChanges {
  trackId: number;
  sequenceId: number;
  position: SequencePosition;
  notes?: NotesMap;
}

// PianoRoll
export interface PianoRollKey {
  keyName: string;
  caption: string;
  color: 'black'|'white';
}


/* INSTRUMENTS */

export interface InstrumentSetting {
  id?: number;
  name?: string;
}

export enum Instrument {
  SAMPLE = 'sample',
  SYNTH = 'synth',
}

export type Instruments = {
  [instr in Instrument]?: InstrumentGroup<InstrumentSetting>;
};

// Sample
export interface AudioBuffers {
  [sampleId: number]: Sample;
}

export interface Sample extends InstrumentSetting {
  audioBuffer: AudioBuffer;
}

// Synths
export interface InstrumentGroup<T> {
  [instrumentId: number]: T;
}

export interface Synth extends InstrumentSetting {
  type?: OscillatorType;
  /** cents, -100 to 100 */
  detune?: number;
  adsr?: Adsr;
}

/** seconds */
type Attack = number;
/** seconds */
type Decay = number;
/** gain */
type Sustain = number;
/** seconds */
type Release = number;
export type Adsr = [Attack, Decay, Sustain, Release];


/* MIXER AND CHANNELS */

export enum ChannelType {
  TRACK = 'track',
  GROUP = 'group',
  FX = 'fx',
  MASTER = 'MASTER',
}

export type MixerChannelsMap = Map<string, MixerChannel>;

export interface MixerChannel {
  /** `${ChannelType}_${id}` */
  channelId: string;
  channelName: string;
  gain: number;
  muted: boolean;
  fx: string[];
  inputGainNode: GainNode;
  analyserNode: AnalyserNode;
}

export interface ChannelSettings {
  name?: string;
  gain?: number;
  muted?: boolean;
}


/* MISC */

export interface Option<T> {
  caption: string;
  value: T;
}

export interface AudioBufferInitData {
  name: string;
  dataUrl: string;
}

export interface SelectedInstrument {
  instrument: Instrument;
  instrumentId: number;
}
