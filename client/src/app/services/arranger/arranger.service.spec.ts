import { TestBed } from '@angular/core/testing';

import { ArrangerService } from './arranger.service';

describe('ArrangerService', () => {
  let service: ArrangerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ArrangerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
