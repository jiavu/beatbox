import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { ArrangementMap } from 'src/app/models/models';

@Injectable({
  providedIn: 'root'
})
export class ArrangerService {

  arrangementMap: ArrangementMap = new Map();
  updateArrangementSubject = new ReplaySubject<[number, number][]>(1);

  constructor() { }

  getUpdateArrangement$(): Observable<[number, number][]> {
    return this.updateArrangementSubject.pipe(
      map(arrangement => {
        this.arrangementMap = new Map(arrangement);
        return arrangement;
      })
    );
  }
}
