import { TestBed } from '@angular/core/testing';

import { AudioBufferLoaderService } from './audio-buffer-loader.service';

describe('AudioBufferLoaderService', () => {
  let service: AudioBufferLoaderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AudioBufferLoaderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
