import { Injectable } from '@angular/core';
import { BehaviorSubject, forkJoin, Observable } from 'rxjs';
import { map, mergeMap, take } from 'rxjs/operators';
import { Utils } from 'src/app/common/utilities';

import { AudioBufferInitData, AudioBuffers } from '../../models/models';
import { MixerService } from '../mixer/mixer.service';

@Injectable({
  providedIn: 'root'
})
export class AudioBufferLoaderService {
  readonly prefix: 'data:';
  readonly mimeType = 'audio/ogg';
  readonly token = ';base64,';

  private audioBuffersSubject = new BehaviorSubject<AudioBuffers>(null);
  private audioBuffers: AudioBuffers;

  constructor(private readonly mixerService: MixerService) {}

  getAudioBuffers$(): Observable<AudioBuffers> {
    return this.audioBuffersSubject.asObservable();
  }

  createAudioBuffer(usvStringOrRequest: string|Request): Promise<AudioBuffer> {
    return fetch(usvStringOrRequest).then(response => {
      if (!response.ok) throw new Error('error: ' + response.status);
      return response.arrayBuffer();
    }).then(arrayBuffer => this.mixerService.audioCtx.decodeAudioData(arrayBuffer));
  }

  /* getDataUrl(file: File): Promise<string|ArrayBuffer> {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  } */

  addAudioBuffers(initData: AudioBufferInitData[]): void {
    this.getAudioBuffers$().pipe(
      take(1),
      mergeMap(buffers => {
        const ids = Object.keys(buffers || {}).map(id => +id);

        return forkJoin(
          initData.map(data => {
            return this.createAudioBuffer(data.dataUrl).then(audioBuffer => {
              const id = Utils.findNewId(ids);
              ids.push(id);
              return { audioBuffer, name: data.name, id };
            });
          })
        ).pipe(
          map(newAudioBuffers => {
            const newAudioBufferCollection: AudioBuffers = buffers || {};

            newAudioBuffers.forEach((entry, idx) => {
              newAudioBufferCollection[entry.id] = entry;
            });

            return newAudioBufferCollection;
          }),
        );
      })
    ).subscribe(newAudioBuffers => {
      this.audioBuffers = newAudioBuffers;
      this.audioBuffersSubject.next(newAudioBuffers);
    });
  }

  playAudioBuffer(instrumentId: number, destination: GainNode): void {
    const sample = this.audioBuffers[instrumentId];
    const source = this.mixerService.audioCtx.createBufferSource();
    source.buffer = sample.audioBuffer;
    source.connect(destination);
    source.start();
  }
}
