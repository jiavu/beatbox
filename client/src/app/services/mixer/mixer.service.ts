import { Injectable } from '@angular/core';
import { BehaviorSubject, iif, Observable, of, throwError } from 'rxjs';
import { map, mergeMap, take } from 'rxjs/operators';
import { Utils } from 'src/app/common/utilities';

import { ChannelSettings, ChannelType, MixerChannel, MixerChannelsMap } from 'src/app/models/models';

@Injectable({
  providedIn: 'root'
})
export class MixerService {
  public readonly audioCtx = new AudioContext();
  private readonly analyzerFftSize = 256;
  private readonly analyzerMaxDecibels = 0;
  private readonly analyzerMinDecibels = -70;

  mixerChannelsMapSubject = new BehaviorSubject<MixerChannelsMap>(new Map());
  mixerChannelsMap$ = this.mixerChannelsMapSubject.asObservable();

  constructor() { }

  addMixerChannel(type: ChannelType, id?: number, settings?: ChannelSettings): Observable<MixerChannel> {
    return this.mixerChannelsMap$.pipe(
      take(1),
      mergeMap(channels => {
        const masterChannel = channels.get(ChannelType.MASTER);

        return iif(
          () => !!masterChannel,
          of({masterChannel, channels}),
          this.getNewMixerChannel(ChannelType.MASTER).pipe(
            map(master => {
              master.analyserNode.fftSize = this.analyzerFftSize;
              master.analyserNode.maxDecibels = this.analyzerMaxDecibels;
              master.analyserNode.minDecibels = this.analyzerMinDecibels;

              master.inputGainNode.connect(master.analyserNode);
              master.inputGainNode.connect(this.audioCtx.destination);
              channels.set(master.channelId, master);

              return {masterChannel: master, channels};
            }),
          ),
        );
      }),
      mergeMap(({masterChannel, channels}) => {
        return this.getNewMixerChannel(type, id, settings).pipe(
          map(newChannel => {
            newChannel.analyserNode.fftSize = this.analyzerFftSize;
            newChannel.analyserNode.maxDecibels = this.analyzerMaxDecibels;
            newChannel.analyserNode.minDecibels = this.analyzerMinDecibels;

            newChannel.inputGainNode.connect(newChannel.analyserNode);
            newChannel.analyserNode.connect(masterChannel.inputGainNode);
            channels.set(newChannel.channelId, newChannel);

            this.mixerChannelsMapSubject.next(channels);
            return newChannel;
          }),
        );
      }),
    );
  }

  getNewMixerChannel(type: ChannelType, id?: number | string, settings: ChannelSettings = {}): Observable<MixerChannel> {
    if (ChannelType.TRACK === type && !id) return throwError(Error('id missing'));
    if (ChannelType.MASTER === type) id = ChannelType.MASTER;

    return iif(
      () => [ChannelType.TRACK, ChannelType.MASTER].includes(type),
      of(id),
      this.findNewId(type).pipe(take(1)),
    ).pipe(map(newId => {
        const channelId = ChannelType.MASTER === newId ? newId : `${type}_${newId}`;
        const channelName = settings.name || (
          ChannelType.MASTER === newId ?
            ChannelType.MASTER : 'CH ' + channelId
        );
        const inputGainNode = this.audioCtx.createGain();
        const gain = settings.muted ? 0 : (settings.gain || 1);
        inputGainNode.gain.setValueAtTime(gain, this.audioCtx.currentTime);

        return {
          channelId,
          channelName,
          gain: settings.gain || 1,
          muted: !!settings.muted,
          inputGainNode,
          analyserNode: this.audioCtx.createAnalyser(),
          fx: [],
        };
    }));
  }

  findNewId(type: ChannelType): Observable<number> {
    return this.mixerChannelsMap$.pipe(
      map(channels => {
        const currentChannelIds = Array.from(channels).filter(([key, value]) => {
          return key.startsWith(type);
        }).map(([key, value]) => +key.split('_')[1]);

        return Utils.findNewId(currentChannelIds);
      }),
    );
  }

  deleteMixerChannel(channelId: string): void {
    this.mixerChannelsMap$.pipe(take(1)).subscribe(channels => {
      channels.delete(channelId);

      this.mixerChannelsMapSubject.next(channels);
    });
  }

  changeMute(channelId: string, isMute: boolean): void {
    this.mixerChannelsMap$.pipe(take(1)).subscribe(channels => {
      const channel = channels.get(channelId);
      channel.muted = isMute;
      channel.inputGainNode.gain.setValueAtTime(
        isMute ? 0 : channel.gain,
        this.audioCtx.currentTime
        );
      this.mixerChannelsMapSubject.next(channels);
    });
  }

  changeTrackVolume(channelId: string, value: number): void {
    this.mixerChannelsMap$.pipe(take(1)).subscribe(channels => {
      const channel = channels.get(channelId);
      channel.gain = value;

      channel.inputGainNode.gain.setValueAtTime(value, this.audioCtx.currentTime);
    });
  }

  getMixerChannelsForSave(): Observable<MixerChannel[]> {
    return this.mixerChannelsMap$.pipe(
      map(mixerChannelsMap => Array.from(mixerChannelsMap)
        .map(([channelId, channel]) => {
          const copy = {...channel};
          delete copy.analyserNode;
          delete copy.inputGainNode;
          return copy;
        })
      ),
    );
  }
}
