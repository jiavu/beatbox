import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { SequencerSetup, Track } from '../../models/models';

interface Positions {
  seqPos: string;
  arrangerPos: number;
}


@Injectable({
  providedIn: 'root'
})
export class SequencerService {
  pianoRollOpenSubject = new BehaviorSubject<Track>(null);
  pianoRollOpen$ = this.pianoRollOpenSubject.asObservable();
  selectedSequenceSubject = new BehaviorSubject<number>(1);
  selectedSequence$ = this.selectedSequenceSubject.asObservable();

  positionMarkerRefs: Map<string, HTMLElement> = new Map();

  constructor() { }

  getPositions(setup: SequencerSetup, elapsedTime: number): Positions {
    const {bpm, measure, bars} = setup;

    const beatDuration = (1000 * 60 / bpm) * 4 / measure.noteValue;
    const barDuration = beatDuration * measure.beats;
    const barProgress = elapsedTime / barDuration;
    const seqPosBar = Math.floor(barProgress % bars) + 1;

    const barRemainder = elapsedTime % barDuration;
    const beatProgress = barRemainder / beatDuration;
    const seqPosBeat = Math.floor(beatProgress) + 1;

    const beatRemainder = barRemainder % beatDuration;
    const fractDuration = beatDuration / 4;
    const fractProgress = beatRemainder / fractDuration;
    const seqPosFract = Math.floor(fractProgress) + 1;

    return {
      seqPos: [seqPosBar, seqPosBeat, seqPosFract].join('.'),
      arrangerPos: Math.floor(barProgress / bars) + 1,
    };
  }

  /**
   * @returns left position with unit %
   */
  calcMarkerPosition(setup: SequencerSetup, elapsedTime: number): string {
    const {bpm, measure, bars} = setup;
    const beatDuration = (1000 * 60 / bpm) * 4 / measure.noteValue;
    const barsDuration = beatDuration * measure.beats * bars;
    const markerProgress = elapsedTime % barsDuration;

    return markerProgress * 100 / barsDuration + '%';
  }
}
