import { Injectable } from '@angular/core';
import { BehaviorSubject, combineLatest, forkJoin, Observable, of } from 'rxjs';
import { map, mergeMap, take } from 'rxjs/operators';
import { Utils } from 'src/app/common/utilities';

import {
  ChannelSettings,
  ChannelType,
  Instrument,
  Note,
  NotesMap,
  SelectedInstrument,
  SessionSettings,
  InstrumentGroup,
  TrackStatesMap,
  InstrumentSetting,
  Instruments,
  TrackSequenceMap,
} from '../../models/models';
import { ArrangerService } from '../arranger/arranger.service';
import { MixerService } from '../mixer/mixer.service';
import { SequencerService } from '../sequencer/sequencer.service';
import { SynthService } from '../synth/synth.service';
import { TracksService } from '../tracks/tracks.service';


@Injectable({
  providedIn: 'root'
})
export class SessionService {
  sessionSettingsSubject = new BehaviorSubject<SessionSettings>(null);
  sessionSettings$ = this.sessionSettingsSubject.asObservable();
  instrumentsSubject = new BehaviorSubject<Instruments>({});
  openInstrumentEditorSubject = new BehaviorSubject<SelectedInstrument>(null);
  openInstrumentEditor$ = this.openInstrumentEditorSubject.asObservable();
  sequenceIdsSubject = new BehaviorSubject<number[]>([]);
  sequenceIds$ = this.sequenceIdsSubject.asObservable();

  private readonly defaultSessionSettings: SessionSettings = {
    name: 'Example session setting',
    bpm: 120,
    bars: 1,
    measure: {beats: 4, noteValue: 4},
    mixerChannels: [],
    tracks: [],
    instruments: {},
    sequenceIds: [1],
    arrangement: [],
  };
  public readonly validationConf = {
    maxBars: 8,
    minBpm: 20,
    maxBpm: 300,
  };

  constructor(
    private readonly arrangerService: ArrangerService,
    private readonly mixerService: MixerService,
    private readonly synthService: SynthService,
    private readonly sequencerService: SequencerService,
    private readonly tracksService: TracksService,
  ) { }

  requestSessionSettings(newSession?: boolean): Observable<SessionSettings> {
    if (newSession || !localStorage.getItem('beatBoxSession')) {
      return this.resetSessionSettings();
    }

    const savedSession = (() => {
      try {
        const parsed = JSON.parse(localStorage.getItem('beatBoxSession')) as SessionSettings;
        return Utils.sessionDataisValid(parsed, this.validationConf) ?
          parsed : Error('Invalid session data.');
      }
      catch {
        const error = Error('couldn\'t parse string');
        console.log(error);
        return error;
      }
    })();

    if (savedSession instanceof Error) {
      console.error(savedSession);
      return this.resetSessionSettings();
    }

    return this.initializeSessionSettings(savedSession);
  }

  /**
   * Sets instruments, creates mixerChannels, creates tracks.
   * For tracks, adds the inputGainNode of the
   * associated mixerChannel as destinationNode to the track object.
   */
  initializeSessionSettings(sessionSettings: SessionSettings): Observable<SessionSettings> {
    // set instruments
    Object.entries<InstrumentGroup<unknown>>(sessionSettings.instruments)
      .forEach(([instrument, group]) => {

      Object.values(group).forEach(instrSetting => {
        this.updateInstrument(instrument as Instrument, instrSetting);
      });

      switch (instrument) {
        case Instrument.SYNTH:
          this.synthService.updateSynths(group);
      }
    });

    // set Arrangement
    this.arrangerService.updateArrangementSubject.next(sessionSettings.arrangement);

    // set SequenceIds
    this.sequenceIdsSubject.next(sessionSettings.sequenceIds);

    // create mixerChannels
    const sourcesArray = sessionSettings.mixerChannels.map(channel => {
      let type: ChannelType;
      let id: number;
      const settings: ChannelSettings = {
        name: channel.channelName,
        gain: channel.gain,
        muted: channel.muted,
      };

      if (ChannelType.MASTER === channel.channelId) {
        type = ChannelType.MASTER;
      } else {
        type = channel.channelId.split('_')[0] as ChannelType;
        id = +channel.channelId.split('_')[1];
      }

      return this.mixerService.addMixerChannel(type, id, settings);
    });

    return !sourcesArray.length ? of(sessionSettings) : forkJoin(sourcesArray).pipe(
      map(mixerChannels => {
        const mixerChannelsMap = new Map(
          mixerChannels.map(channel => [channel.channelId, channel])
        );
        this.mixerService.mixerChannelsMapSubject.next(mixerChannelsMap);

        // Create sequences Maps
        sessionSettings.tracks.forEach(track => {
          track.sequences = new Map(
            (track.sequences as unknown as [number, [string, NotesMap][]][]).map(seqEntry => {

              const sequenceMap: TrackSequenceMap = new Map(
                seqEntry[1].map(entry => [entry[0], new Map(entry[1])])
              );

              return [seqEntry[0], sequenceMap];
            })
          );

          track.destinationNode = mixerChannelsMap.get(
            `${ChannelType.TRACK}_${track.trackId}`
            ).inputGainNode;
        });

        // set tracks
        this.tracksService.trackStatesMap = this.tracksService.createTrackStatesMap(sessionSettings.tracks);
        this.tracksService.tracksChangesSubject.next();

        return sessionSettings;
      })
    );
  }

  /**
   * Resets everything and returns default session settings.
   */
  resetSessionSettings(): Observable<SessionSettings> {
    this.mixerService.mixerChannelsMapSubject.next(new Map());

    this.synthService.updateSynths({});
    this.instrumentsSubject.next({});

    this.tracksService.trackStatesMap = new Map();
    // because number of tracks changes:
    this.tracksService.tracksChangesSubject.next();

    this.sequenceIdsSubject.next([1]);
    this.sequencerService.selectedSequenceSubject.next(1);

    return this.initializeSessionSettings(this.defaultSessionSettings);
  }

  /**
   * Adds a new sequence to trackStates. If removId is given,
   * will remove sequence of removeId instead.
   */
  addOrRemoveSequence(trackStatesMap: TrackStatesMap, removeId?: number): Promise<TrackStatesMap> {
    return this.sequenceIds$.pipe(
      take(1),
      map(seqIds => {
        if ('number' === typeof removeId) {
          trackStatesMap.forEach((track, key) => {
            track.sequences.delete(removeId);
          });

        } else {
          const newSeqId = Utils.findNewId(seqIds);
          seqIds.push(newSeqId);
          this.sequencerService.selectedSequenceSubject.next(newSeqId);

          trackStatesMap.forEach((track, key) => {
            track.sequences.set(newSeqId, new Map());
          });
        }

        this.sequenceIdsSubject.next(seqIds);
        return trackStatesMap;
      }),
    ).toPromise();
  }

  // Todo: should move to tracks.service.ts, if possible.
  deleteTrack(trackStatesMap: TrackStatesMap, trackId: number): TrackStatesMap {
    const track = trackStatesMap.get(trackId);
    if (track) {
      this.deleteInstrument(Instrument.SYNTH, track.instrumentId);
      this.mixerService.deleteMixerChannel(`${ChannelType.TRACK}_${trackId}`);
      trackStatesMap.delete(trackId);
    }
    return trackStatesMap;
  }

  updateInstrument(instr: Instrument, setting: InstrumentSetting): void {
    this.instrumentsSubject.pipe(take(1)).subscribe(instruments => {

      instruments[instr] = instruments[instr] || {};
      instruments[instr][setting.id] = setting;
      this.instrumentsSubject.next(instruments);
    });
  }

  deleteInstrument(instr: Instrument, id: number): void {
    this.instrumentsSubject.pipe(take(1)).subscribe(instruments => {

      delete instruments[instr][id];

      switch (instr) {
        case Instrument.SYNTH:
          this.synthService.deleteSynth(id);
          break;
      }
    });
  }

  saveSession(): void {
    this.sessionSettings$.pipe(

      // TODO: Umbauen, ich möchte unnötige Observables und Subjects
      // abschaffen. Werden diese pipes hier noch notwendig sein?
      mergeMap(sessionSettings => combineLatest([
        of(sessionSettings as any),

        of(this.tracksService.getTracksForSave()).pipe(
          // not sure if I need to clone everything if I use map here (changed it lately).
          map(tracks => tracks.map(track => {
            const trackShallowClone = {...track} as any;
            delete trackShallowClone.destinationNode;

            // convert sequences and notesMaps....
            const sequencesShallowClone = [...Array.from<[number, TrackSequenceMap]>(track.sequences)]
              .map(([seqId, sequencesMap]) => {
                const sequenceShallowClone = [...Array.from<[string, NotesMap]>(sequencesMap)]
                  .map(([pos, notesMap]) => {
                    return [pos, [...Array.from<[string, Note]>(notesMap)]];
                  });
                return [seqId, sequenceShallowClone];
              });

            trackShallowClone.sequences = sequencesShallowClone;

            return trackShallowClone;
          })),
        ),

        this.mixerService.getMixerChannelsForSave(),
        this.instrumentsSubject,
        this.sequenceIdsSubject,
        of(this.arrangerService.arrangementMap),
      ])
      ),
      take(1),

    ).subscribe(([sessionSettings, tracks, mixerChannels, instruments, seqIds, arrangement]) => {
      sessionSettings = {
        ...sessionSettings,
        tracks,
        mixerChannels,
        instruments,
        sequenceIds: seqIds,
        arrangement: Array.from(arrangement),
      };

      const settingsClone = JSON.parse(JSON.stringify(sessionSettings));

      localStorage.setItem('beatBoxSession', JSON.stringify(settingsClone));

      this.logSavedSettingsData(settingsClone);
    });
  }

  logSavedSettingsData(settings: any): void {
    console.log('%csaved project settings as "beatBoxSession":', 'color: green;');
    const table: any = {
      name: settings.name,
      bars: settings.bars,
      bpm: settings.bpm,
      measure: `${settings.measure.beats}/${settings.measure.noteValue}`,
      tracks: settings.tracks.length,
      mixerChannels: settings.mixerChannels.length,
      muted: settings.mixerChannels.filter(chnl => chnl.muted).length,
      ['instrumentGroups:']: Object.keys(settings.instruments).length,
      sequenceIds: settings.sequenceIds.length,
    };
    Object.entries(settings.instruments).forEach(([group, entries]) => {
      table[group + 's'] = Object.keys(entries).length;
    });

    console.table(table);
    console.log(settings);
  }
}
