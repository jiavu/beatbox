import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, take, tap } from 'rxjs/operators';
import { Utils } from 'src/app/common/utilities';

import { Note, InstrumentGroup, Synth } from '../../models/models';
import { MixerService } from '../mixer/mixer.service';

@Injectable({
  providedIn: 'root'
})
export class SynthService {
  public readonly maxGain = .5;  // 1 is to loud!

  private readonly defaultSynth: Synth = {
    name: 'New Synth',
    type: 'sine',
    adsr: [.01, .1, this.maxGain, .1],
  };

  synthsSubject = new BehaviorSubject<InstrumentGroup<Synth>>({});
  private synths: InstrumentGroup<Synth>;


  constructor(private readonly mixerService: MixerService) {}

  updateSynths(synths: InstrumentGroup<Synth>): void {
    this.synths = synths;
    this.synthsSubject.next(synths);
  }

  getSynths$(): Observable<InstrumentGroup<Synth>> {
    return this.synthsSubject.pipe(
      tap(() => console.log('side effect getSynths$')),
      tap(console.log)
    );
  }

  addSynth(): Observable<Synth> {
    return this.getSynths$().pipe(
      take(1),
      map(synths => {
        const newId = Utils.findNewId(Object.keys(synths).map(key => +key));
        synths[newId] = {...this.defaultSynth, id: newId};

        this.updateSynths(synths);
        return synths[newId];
      }),
    );
  }

  deleteSynth(instrumentId: number): void {
    this.getSynths$().pipe(take(1)).subscribe(synths => {
      delete synths[instrumentId];
      this.updateSynths(synths);
    });
  }

  changeSynthSetting(newSetting: Synth): void {
    this.getSynths$().pipe(take(1)).subscribe(synths => {
      synths[newSetting.id] = { ...synths[newSetting.id], ...newSetting };
      this.updateSynths(synths);
    });
  }

  playOscillator(instrumentId: number, note: Note, destination: GainNode): void {
    if (!note.frequency) return;

    const synthSetting = this.synths[instrumentId];
    const {adsr} = synthSetting;

    if (this.maxGain < adsr[2]) adsr[2] = this.maxGain;

    const ctx = this.mixerService.audioCtx;
    const osc = ctx.createOscillator();
    const adsrGainNode = ctx.createGain();
    osc.type = synthSetting.type;
    osc.frequency.setValueAtTime(note.frequency, ctx.currentTime);

    if (synthSetting.detune) {
      osc.detune.setValueAtTime(synthSetting.detune, ctx.currentTime);
    }

    osc.connect(adsrGainNode);
    adsrGainNode.gain.setValueAtTime(.01, ctx.currentTime);
    // adsrGainNode.gain.linearRampToValueAtTime(1, ctx.currentTime + adsr[0]);
    adsrGainNode.gain.exponentialRampToValueAtTime(this.maxGain, ctx.currentTime + adsr[0]);
    adsrGainNode.gain.exponentialRampToValueAtTime(adsr[2], ctx.currentTime + adsr[0] + adsr[1]);
    adsrGainNode.gain.exponentialRampToValueAtTime(.01, ctx.currentTime + adsr[0] + adsr[1] + note.length + adsr[3]);

    adsrGainNode.connect(destination);
    osc.start();
    osc.stop(ctx.currentTime + adsr[0] + adsr[1] + note.length + adsr[3]);
  }
}
