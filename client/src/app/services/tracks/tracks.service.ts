import { Injectable } from '@angular/core';
import { Observable, of, ReplaySubject } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Track, TrackStatesMap } from 'src/app/models/models';


@Injectable({
  providedIn: 'root'
})
export class TracksService {
  trackStatesMap: TrackStatesMap = new Map();
  /** update only if number of tracks changes. */
  tracksChangesSubject = new ReplaySubject(1);

  constructor() { }

  getTracksChanges$(): Observable<TrackStatesMap> {
    return this.tracksChangesSubject.asObservable().pipe(
      switchMap(() => of(this.trackStatesMap)),
    );
  }

  createTrackStatesMap(tracks: Track[]): TrackStatesMap {
    const trackStates = new Map();

    tracks.sort((a, b) => a.position - b.position).forEach(track => {
      trackStates.set(track.trackId, track);
    });

    return trackStates;
  }

  deleteTrack() {
    // todo. Need to update instruments and mixerChannels as
    // well. Need to be careful with cyclic dependencies.
  }

  getTracksForSave(): Track[] {
    return Array.from(this.trackStatesMap).map(([id, track]) => track);
  }
}
