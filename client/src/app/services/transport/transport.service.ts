import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TransportService {
  playSubject = new Subject<boolean>();
  play$ = this.playSubject.asObservable();

  elapsedTime: number;
  sequencePosition: string;
  elapsedTimeDisplayRef: HTMLElement;
  seqPosRef: HTMLElement;

  constructor() { }

}
