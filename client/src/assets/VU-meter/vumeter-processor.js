// modified version of: https://www.w3.org/TR/webaudio/#vu-meter-mode

const SMOOTHING_FACTOR = 0.9;
const MINIMUM_VALUE = 0.00001;

registerProcessor('vumeter', class extends AudioWorkletProcessor {

  constructor (options) {
    super();
    this._volume = 0;
    this._updateIntervalInMS = options.processorOptions.updateIntervalInMS;
    this._mode = options.processorOptions.mode;
    this._nextUpdateFrame = this._updateIntervalInMS;  // ?
    // this._nextUpdateFrame = this.intervalInFrames;

    this.port.onmessage = event => {
      if (event.data.updateIntervalInMS)
        this._updateIntervalInMS = event.data.updateIntervalInMS;
    }
  }

  get intervalInFrames () {
    return this._updateIntervalInMS / 1000 * sampleRate;
  }

  process (inputs, outputs, parameters) {
    const input = inputs[0];

    // Note that the input will be down-mixed to mono; however, if no inputs are
    // connected then zero channels will be passed in.

    // if (input.length > 0) {

      // possibility to get 2 channels of Stereo?
      const samples = input[0];
      let volume = 0;

      if (samples) {
        switch (this._mode) {
          case 'maxPeak':
            volume = Math.max(...samples);
            break;
          case 'rms':
            // Calculate the squared-sum.
            const squaredSum = samples.map(sample => sample ** 2).reduce((a, b) => a + b);
            // Calculate the RMS level.
            volume = Math.sqrt(squaredSum / samples.length);
            break;
        }
      }

      //  Update the volume.
      this._volume = Math.max(volume, this._volume * SMOOTHING_FACTOR);
      if (MINIMUM_VALUE > this._volume) this._volume = 0;

      // Update and sync the volume property with the main thread.
      this._nextUpdateFrame -= samples ? samples.length : 128;

      if (this._nextUpdateFrame < 0) {
        this._nextUpdateFrame += this.intervalInFrames;
        this.port.postMessage({ volume: this._volume });
      }

    // }

    return true;

    // Keep on processing if the volume is above a threshold, so that
    // disconnecting inputs does not immediately cause the meter to stop
    // computing its smoothed value.
    // return this._volume >= MINIMUM_VALUE;
  }
});