https://medium.com/@richardr39/using-angular-cli-to-serve-over-https-locally-70dab07417c8

(Jun 3, 2018)

# 1. Create certificate
```
[req]
default_bits = 2048
prompt = no
default_md = sha256
x509_extensions = v3_req
distinguished_name = dn
[dn]
C = GB
ST = London
L = London
O = My Organisation
OU = My Organisational Unit
emailAddress = email@domain.com
CN = localhost
[v3_req]
subjectAltName = @alt_names
keyUsage = critical, digitalSignature, keyAgreement
extendedKeyUsage = serverAuth
[alt_names]
DNS.1 = localhost
IP.1 = 127.0.0.1
```
Generate certificate and key:

`
openssl req -new -x509 -newkey rsa:2048 -sha256 -nodes -keyout localhost.key -days 3560 -out localhost.crt -config certificate.cnf
`

Configure Angular: *angular.json*
```
"serve": {
  "builder": ...,
  "options": {
    "browserTarget": ...,
    "ssl": true,            // if ommited, execute ng serve --ssl instead
    "sslCert": pathTo/localhost.crt,
    "sslKey": pathTo/localhost.key
  }
}
```
# 2. Trusting the certificate on Windows
Add localhost.crt to the Trusted Root Certification Authorities store.

- Windows + R
- run mmc
- File > Add/Remove Snap-in...
- Add Certificates (for current user) and close
- Trusted Root Certification Authorities > Certificates
- Right click
- Akk Tasks > Import...
- import localhost.crt
- Close mmc (saving the console isn't necessary)
- Restart Chrome